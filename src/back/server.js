const express = require('express')
const path = require('path');
const port = process.argv[2] || process.env.PUNK_PORT
const host = process.argv[3] || process.env.PUNK_HOST

function Server (dirname) {
  const app = express()

  console.log()
  app.use('/static', express.static(path.join(dirname, 'assets')))

  app.get('/', (req, res) => {
    res.sendFile(path.join(dirname, 'assets', 'index.html'))
  })

  /*app.get('/images/:file', (req, res) => {
    res.sendFile(path.join(dirname, 'assets', 'images', req.params.file))
  })*/
  app.get('/:file', (req, res) => {
    res.sendFile(path.join(dirname, 'assets', 'scripts', req.params.file))
  })


  app.listen(port, () => {
    console.log(`Server started on port ${port}`)
    console.log(`To launch the game go to ${host}`)
  })
}

module.exports = {
  Server
}
