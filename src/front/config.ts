export const GAME_WIDTH = 800;
export const GAME_HEIGHT = 600;
export const FRAME_RATE = 10;

export const LOAD_URL = "/";
export const IMAGE_URL = `${__dirname}/images/`;

export const BACKGROUND_MENU = "background-menu.jpg";
export const BACKGROUND = "background.jpg";
export const PLATFORM = "platform.png";

export const GameConfig: Phaser.IGameConfig = {
  antialias: false,
  height: GAME_HEIGHT,
  parent  : "",
  renderer: Phaser.AUTO,
  width: GAME_WIDTH,
};
