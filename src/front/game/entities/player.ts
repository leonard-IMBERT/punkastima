import { Sprite } from "phaser-ce";
import SpriteConfig from "../../tools";
import Buff from "../mechanics/buffs";
import Movable from "./moveable.i";

export enum Facing {
    RIGHT,
    LEFT,
}

export default class Player extends Phaser.Sprite implements Movable {

    public static readonly SPRITE_LOCATION = "entities/player/player.png";

    public static readonly SPRITE_KEY: string = "player_sprite";

    public static readonly LEFT_MOV: SpriteConfig = new SpriteConfig("player_left_mov", [ 34, 33, 32]);
    public static readonly RIGHT_MOV: SpriteConfig = new SpriteConfig("player_right_mov", [ 1, 2, 3]);
    public static readonly LEFT_FIGHT: SpriteConfig =
        new SpriteConfig("player_left_fight", [ 25, 24, 23, 22, 21, 20, 19]);
    public static readonly RIGHT_FIGHT: SpriteConfig =
        new SpriteConfig("player_right_fight", [ 11, 12, 13, 14, 15, 16, 17]);
    public static readonly STAND_RIGHT: SpriteConfig =
        new SpriteConfig("player_right_stand", [0]);
    public static readonly STAND_LEFT: SpriteConfig =
        new SpriteConfig("player_left_stand", [35]);

    public static readonly STANDARD_VELOCITY: number = 150;
    public static readonly STANDARD_JUMP: number = 450;

    public static readonly GRAVITY: number = 1000;

    public score: number = 0;
    public buffs: Buff[] = [];

    private facing: Facing = Facing.RIGHT;

    constructor(
        game: Phaser.Game,
        x: number,
        y: number,
        group?: Phaser.Group | Phaser.Stage,
        frame?: string | number,
    ) {
        super(game, x, y, Player.SPRITE_KEY, frame);

        if (group) { group.add(this); } else { this.game.world.add(this); }
        this.game.physics.arcade.enable(this);
        this.scale.setTo(2);

        this.body.gravity.y = Player.GRAVITY;
        this.body.collideWorldBounds = true;

        Player.LEFT_MOV.register(this.animations, true);
        Player.RIGHT_MOV.register(this.animations, true);
        Player.LEFT_FIGHT.register(this.animations, true);
        Player.RIGHT_FIGHT.register(this.animations, true);
        Player.STAND_RIGHT.register(this.animations, false);
        Player.STAND_LEFT.register(this.animations, false);
    }

    public onMovementRight() {
        this.facing = Facing.RIGHT;
        this.body.velocity.x = Player.STANDARD_VELOCITY;
        this.animations.play(Player.RIGHT_MOV.key);
    }

    public onMovementLeft() {
        this.facing = Facing.LEFT;
        this.body.velocity.x = -Player.STANDARD_VELOCITY;
        this.animations.play(Player.LEFT_MOV.key);
    }

    public onStop() {
        this.body.velocity.x = 0;
        switch (this.facing) {
            case Facing.RIGHT:
                this.animations.play(Player.STAND_RIGHT.key);
                break;
            case Facing.LEFT:
                this.animations.play(Player.STAND_LEFT.key);
                break;
            default:
        }
    }

    public onJump() {
        this.body.velocity.y = -Player.STANDARD_JUMP;
    }
    public onHit() {
        throw new Error("Method not implemented.");
    }
    public onDeath() {
        throw new Error("Method not implemented.");
    }

    public update() {
        this.buffs.forEach((element: Buff) => {
            if (element.onNext(this)) { this.buffs.splice(this.buffs.indexOf(element), 1); }
            this.score = element.time;
        });
    }
}
