export default interface IMovable {
    onMovementRight(): void;
    onMovementLeft(): void;
    onStop(): void;
    onJump(): void;
    onHit(): void;
    onDeath(): void;
}
