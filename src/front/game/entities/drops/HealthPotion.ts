import Buff from "../../mechanics/buffs";
import Player from "../player";
import IDropable from "./dropable.i";

export default class HealthPotion extends Phaser.Sprite implements IDropable {

    public static readonly SPRITE_LOCATION = "entities/dropable/potion.png";
    public static readonly SPRITE_KEY = "healthpotion_sprite";
    constructor(
        game: Phaser.Game,
        x: number,
        y: number,
        group?: Phaser.Group | Phaser.Stage,
        frame?: string | number,
    ) {
        super(game, x, y, HealthPotion.SPRITE_KEY, frame);
        if (group) { group.add(this); } else { this.game.world.add(this); }
        this.game.physics.arcade.enable(this);
        this.scale.setTo(0.25);

        this.body.gravity.y = Player.GRAVITY;
        this.body.collideWorldBounds = true;
    }

    public pickUp(player: Player): void {
        const buff = new Buff((b: Buff) => (p: Player) => {
            b.state.set("initial_gravity", player.body.gravity.y);
            player.body.gravity.y /= 2;
        }, (b: Buff) => (p: Player) => {
            player.body.gravity.y = b.state.get("initial_gravity");
        }, (b: Buff) => (p: Player) => "", 500);

        buff.onBegin(player);
        player.buffs.push(buff);
        this.kill();
    }
}
