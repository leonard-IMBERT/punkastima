import Player from "../player";

export default interface IDropable {
    pickUp(player: Player): void;
}
