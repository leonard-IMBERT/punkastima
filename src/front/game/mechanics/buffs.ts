import Player from "../entities/player";

export default class Buff {
    public time: number;
    public state: Map<string, any>;
    private effects: (player: Player) => void;
    private reverseEffect: (player: Player) => void;
    private next: (player: Player) => void;

    constructor(effects: (buff: Buff) => (player: Player) => void,
                reverseEffect: (buff: Buff) => (player: Player) => void,
                next: (buff: Buff) => (player: Player) => void,
                time: number) {
        this.time = time;
        this.effects = effects(this);
        this.reverseEffect = reverseEffect(this);
        this.state = new Map<string, any>();
        this.next = next(this);
    }

    public onBegin(player: Player): void {
        this.effects(player);
    }

    public onEnd(player: Player): void {
        this.reverseEffect(player);
    }

    public onNext(player: Player): boolean {
        this.time -= 1;
        if (this.time <= 0) {
            this.onEnd(player);
            return true;
        } else {
            this.onNext(player);
            return false;
        }
    }
}
