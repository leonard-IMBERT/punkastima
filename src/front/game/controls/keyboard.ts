export enum Keys {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    JUMP,
    SHOOT,
    MENU,
    ENTER,
}

export default class KeyBoardManager {
    public game: Phaser.Game;

    private keyMap: Map<Keys, Phaser.Key> = new Map<Keys, Phaser.Key>();

    constructor(game: Phaser.Game) {
        this.game = game;

        this.keyMap.set(Keys.UP, this.game.input.keyboard.addKey(Phaser.KeyCode.Z))
            .set(Keys.DOWN, this.game.input.keyboard.addKey(Phaser.KeyCode.S))
            .set(Keys.LEFT, this.game.input.keyboard.addKey(Phaser.KeyCode.Q))
            .set(Keys.RIGHT, this.game.input.keyboard.addKey(Phaser.KeyCode.D))
            .set(Keys.JUMP, this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR))
            .set(Keys.SHOOT, this.game.input.keyboard.addKey(Phaser.KeyCode.SHIFT))
            .set(Keys.MENU, this.game.input.keyboard.addKey(Phaser.KeyCode.ESC))
            .set(Keys.ENTER, this.game.input.keyboard.addKey(Phaser.KeyCode.ENTER));
    }

    public bind(key: Keys, code: number): void {
        const odlkey: Phaser.Key = this.keyMap.get(key);
        this.keyMap.set(key, this.game.input.keyboard.addKey(code));
        this.game.input.keyboard.removeKey(odlkey.keyCode);
    }

    public key(key: Keys): Phaser.Key {
        return this.keyMap.get(key);
    }
}
