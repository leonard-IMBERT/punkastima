import { BACKGROUND } from "../../config";
import Punkastima from "../Punkastima";

export default class Hub extends Phaser.State {
    constructor(game: Punkastima) {
        super();
        this.game = game;
    }

    public update(): void {
        this.add.image(0, 0, BACKGROUND);
    }
}
