import * as url from "url";
import Punkastima from "../Punkastima";

import HealthPotion from "../entities/drops/HealthPotion";
import Player from "../entities/player";

import {
    BACKGROUND,
    BACKGROUND_MENU,
    GAME_HEIGHT,
    GAME_WIDTH,
    IMAGE_URL,
    PLATFORM,
  } from "../../config";
import Menu from "./menu";
import Demo from "./demo";

export default class Load extends Phaser.State {

    public static readonly LOADING_SENTENCE = "GAME IS LOADING";
    public static readonly STATE_KEY = "state_load";

    public game: Punkastima;

    constructor(game: Punkastima) {
        super();
        this.game = game;
    }

    public preload() {
        this.game.add.text(GAME_HEIGHT / 2 - 16, GAME_WIDTH / 2 - 50, Load.LOADING_SENTENCE,
            { font: "Fira Sans", fontStyle: "monospace", fontSize: 32});

        this.game.load.image(BACKGROUND, url.resolve(IMAGE_URL, BACKGROUND));
        this.game.load.image(PLATFORM, url.resolve(IMAGE_URL, PLATFORM));
        this.game.load.spritesheet(Player.SPRITE_KEY, url.resolve(IMAGE_URL, Player.SPRITE_LOCATION), 20, 20);
        this.game.load.image(HealthPotion.SPRITE_KEY, url.resolve(IMAGE_URL, HealthPotion.SPRITE_LOCATION));
        this.game.load.image(BACKGROUND_MENU, url.resolve(IMAGE_URL, BACKGROUND_MENU));
    }

    public create() {
        this.game.state.start(Menu.STATE_KEY);
    }
}
