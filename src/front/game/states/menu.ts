import {
    BACKGROUND_MENU, GAME_HEIGHT, GAME_WIDTH,
} from "../../config";

import { Keys } from "../controls/keyboard";
import Punkastima from "../Punkastima";
import Load from "./load";
import Demo from "./demo";

enum MenuState {
    PLAY,
    OPTION,
    EXIT,
}

export default class Menu extends Phaser.State {
    public static readonly TEXT_CONFIG: Phaser.PhaserTextStyle = {
        fill: "#050505",
        font: "64px \"Hammersmith One\", monospace",
        shadowBlur: 3,
        shadowColor: "#000",
        shadowOffsetX: 20,
        shadowOffsetY: 20,
        stroke: "white",
        strokeThickness: 2,
    };

    public static readonly STATE_KEY: string = "state_menu";
    public game: Punkastima;

    private readonly PLAY_HEIGHT = GAME_HEIGHT / 2 - 60;
    private readonly OPTION_HEIGHT = GAME_HEIGHT / 2;
    private readonly EXIT_HEIGHT = GAME_HEIGHT / 2 + 60;

    private menuState: MenuState;
    private selector: Phaser.Text;
    private selectorPressed: boolean = false;

    constructor(game: Punkastima) {
        super();
        this.game = game;
        this.menuState = MenuState.PLAY;
    }

    public create(): void {
        this.game.add.image(0, 0, BACKGROUND_MENU);
        this.game.add.text(GAME_WIDTH / 2 - 175, GAME_HEIGHT / 2 - 120, "Punkastima", Menu.TEXT_CONFIG);

        this.game.add.text(GAME_WIDTH / 2 - 150, this.PLAY_HEIGHT, "Play", Menu.TEXT_CONFIG);
        this.game.add.text(GAME_WIDTH / 2 - 150, this.OPTION_HEIGHT, "Options", Menu.TEXT_CONFIG);
        this.game.add.text(GAME_WIDTH / 2 - 150, this.EXIT_HEIGHT, "Exit", Menu.TEXT_CONFIG);

        this.selector = this.add.text(GAME_WIDTH / 2 - 185, this.PLAY_HEIGHT, ">", Menu.TEXT_CONFIG);
    }

    public update(): void {
        if (this.game.keys.key(Keys.ENTER).isDown) {
            switch (this.menuState) {
                case MenuState.EXIT: {
                    this.game.destroy();
                    break;
                }
                case  MenuState.PLAY: {
                    this.game.state.start(Demo.STATE_KEY, true);
                    break;
                }
            }
        }

        if (this.game.keys.key(Keys.DOWN).isDown && !this.selectorPressed) {
            switch (this.menuState) {
                case MenuState.PLAY:
                    this.menuState = MenuState.OPTION;
                    break;
                default:
                    this.menuState = MenuState.EXIT;
                    break;
            }
            this.selectorPressed = true;
        } else if (this.game.keys.key(Keys.UP).isDown && !this.selectorPressed) {
            switch (this.menuState) {
                case MenuState.EXIT:
                    this.menuState = MenuState.OPTION;
                    break;
                default:
                    this.menuState = MenuState.PLAY;
                    break;
            }
            this.selectorPressed = true;
        } else if (this.game.keys.key(Keys.UP).isUp && this.game.keys.key(Keys.DOWN).isUp) {
            this.selectorPressed = false;
        }

        switch (this.menuState) {
            case MenuState.PLAY:
                this.selector.y = this.PLAY_HEIGHT;
                break;
            case MenuState.OPTION:
                this.selector.y = this.OPTION_HEIGHT;
                break;
            case MenuState.EXIT:
                this.selector.y = this.EXIT_HEIGHT;
                break;
            default:
        }
    }
}
