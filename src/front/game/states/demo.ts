import Player from "../entities/player";
import Punkastima from "../Punkastima";
import { BACKGROUND, PLATFORM } from "../../config";
import HealthPotion from "../entities/drops/HealthPotion";
import IDropable from "../entities/drops/dropable.i";
import { Keys } from "../controls/keyboard";
import Menu from "./menu";
import { triggerAsyncId } from "async_hooks";

export default class Demo extends Phaser.State {

  public static readonly STATE_KEY: string = "state_demo";

  private platforms: Phaser.Group;
  private dropable: Phaser.Group;
  private player: Player;
  private score: Phaser.Text;
  public game: Punkastima;

  constructor(game: Punkastima) {
    super();
    this.game = game;
  }

  public create() {
    this.add.sprite(-50, -50, BACKGROUND);
    this.platforms = this.add.group();
    this.dropable = this.add.group();

    this.platforms.enableBody = true;

    const ground: Phaser.Sprite = this.platforms.create(50, this.world.height - 200, PLATFORM);
    ground.body.immovable = true;

    const potion = new HealthPotion(this.game, 150, 20, this.dropable);

    this.player = new Player(this.game, 100, 20);

    this.score = this.add.text(16, 16, "Score 0", { fontSize: 32, fill: "#000" });

  }

  public update() {
    this.physics.arcade.collide(this.dropable, this.platforms);
    this.physics.arcade.collide(this.player, this.platforms);
    this.physics.arcade.collide(this.player, this.dropable, (player: Player, dropable: IDropable) => {
      dropable.pickUp(player);
    });

    if(this.game.keys.key(Keys.RIGHT).isDown) this.player.onMovementRight();
    else if(this.game.keys.key(Keys.LEFT).isDown) this.player.onMovementLeft();
    else this.player.onStop();

    if(this.game.keys.key(Keys.JUMP).isDown && this.player.body.touching.down) this.player.onJump();

    this.player.update();
    this.score.text = `Score: ${this.player.score}`;

    if(this.game.keys.key(Keys.MENU).isDown) this.game.state.start(Menu.STATE_KEY, true);
  }
}