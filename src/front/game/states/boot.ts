import KeyBoardManager from "../controls/keyboard";
import Punkastima from "../Punkastima";
import Load from "./load";

export default class Boot extends Phaser.State {

    public static readonly STATE_KEY: string = "state_boot";

    public game: Punkastima;

    constructor(game: Punkastima) {
        super();
        this.game = game;
    }

    public create(): void {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.keys = new KeyBoardManager(this.game);

        this.game.state.start(Load.STATE_KEY);
    }
}
