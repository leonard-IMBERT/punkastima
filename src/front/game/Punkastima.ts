// import * as Phaser from "phaser-ce";
import { GameConfig } from "../config";
import KeyBoardManager from "./controls/keyboard";
import Player from "./entities/player";
import Boot from "./states/boot";
import Load from "./states/load";
import Menu from "./states/menu";
import Demo from "./states/demo";

export default class Punkastima extends Phaser.Game {

  public player: Player;
  public keys: KeyBoardManager;

  constructor() {
    super(GameConfig);
    this.state.add(Boot.STATE_KEY, new Boot(this));
    this.state.add(Load.STATE_KEY, new Load(this));
    this.state.add(Demo.STATE_KEY, new Demo(this));
    this.state.add(Menu.STATE_KEY, new Menu(this));

    this.state.start(Boot.STATE_KEY);
  }
}
