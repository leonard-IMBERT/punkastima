import * as p2 from "p2";
import * as PIXI from "pixi";

export default function () {
    window.PIXI = PIXI;
    window.p2 = p2;
}