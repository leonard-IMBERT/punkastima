import init from "./import";
import Electron from "electron";

init();

import "p2"
import "pixi"
import "phaser"
window.onload = (_) => {
  document.fonts.load('1em "Hammersmith One"').then(() => {
    import("./game/Punkastima").then((Punkastima) => {
      const punkastima = new Punkastima.default();
      const canvas = punkastima.canvas;
      const observer = new MutationObserver((e) => {
        if(e[0].removedNodes.length > 0) {
          console.log(Electron.ipcRenderer)
          Electron.ipcRenderer.send('game_close', true)
        }
      });
      observer.observe(document.querySelector('body'), { attributes: true, childList: true });
    });
  });
};
