import * as url from "url";
import {
    BACKGROUND,
    FRAME_RATE,
    IMAGE_URL,
    PLATFORM,
} from "./config";
import HealthPotion from "./game/entities/drops/HealthPotion";
import Player from "./game/entities/player";

export default class SpriteConfig {
    public readonly key: string;
    public readonly frames: number[];

    constructor(key: string, frames: number[]) {
        this.key = key;
        this.frames = frames;
    }

    public register(manager: Phaser.AnimationManager, loop: boolean): void {
        manager.add(this.key, this.frames, FRAME_RATE, loop);
    }
}
