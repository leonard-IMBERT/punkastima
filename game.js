const Electron = require('electron');

/*const { Server } = require('./src/back/server')

Server(__dirname)*/

let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new Electron.BrowserWindow({width: 800, height: 600, frame: false, resizable: false, openDevTools: false})

  // and load the index.html of the Electron.app.
  mainWindow.loadURL(`file://${__dirname}/assets/index.html`)

  Electron.ipcMain.on('game_close', (e) => {
    Electron.app.quit();
  })
  //mainWindow.webContents.on("devtools-opened", () => { mainWindow.webContents.closeDevTools(); });
  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your Electron.app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
Electron.app.on('ready', createWindow)

// Quit when all windows are closed.
Electron.app.on('window-all-closed', function () {
  // On OS X it is common for Electron.applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    Electron.app.quit()
  }
})

Electron.app.on('activate', function () {
  // On OS X it's common to re-create a window in the Electron.app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})